#  YouParty 
<p align="center">
You Party, where you see every thing with your friends in real time
</p>
YouParty é uma aplicação para pessoas que desejam assistir vídeos do youtube com os amigos de forma rápida e fácil. O projeto consiste em utilizar o Socket.io para minimizar as diferenças entre os mecanismos de transportes, tornando possível uma comunicação de forma rápida e eficiente entre o navegador e o servidor.

<p align="center">
<img src="./assets_readme/background.png" width="70%">
</p>

## Technologies
O projeto consiste em utilizar o Socket.io para minimizar as diferenças entre os mecanismos de transportes, tornando possível uma comunicação de forma rápida e eficiente entre o navegador e o servidor. Para o desenvolvimento, o grupo utilizou da tecnologia do Node.js, buscando criar um ambiente mais “leve”, além do grande repositório que a ferramenta possui através do NPM, possibilitando, então, a integração com sistemas e bancos de dados de forma simples, por exemplo. Como framework para o Node.js, o grupo escolheu o Express.js por fornecer os recursos para construção de servidores web.

<p align="center">
<img src="./assets_readme/tech.png" width="70%">
</p>



## 📝 Before Running
1. Make sure **Node.js version 14** is installed on the master machine by running:

```bash
node -v
```
2. The output should look someting like `v14.17.5`, if this is not the case use the following link for tips on how to install it:
[How To Install Node.js on Ubuntu 21](https://tecadmin.net/install-nodejs-ubuntu-non-lts/)
[How To Install Node.js on Windows 11](https://techdecodetutorials.com/web-development/how-to-install-node-js-on-windows-11/)

3. After Node.js is installed, Install pm2 on master machine. Run command:
```bash
sudo npm i -g pm2
```

4. It must be a browser based on the chromium browser project. Thus, Firefox may exhibit instability.
<p align="center">
<img src="./assets_readme/chromium.png" width="70%">
</p>

## Requirements to install YouParty locally

The machine must be connected to the internet.

First of all, Git must me installed. Open the terminal pressing CTRL + ALT + T or by searching at Activities tab, found at the top left corner of the screen.

Then, use `sudo apt install -y git` ( If you are using Ubuntu System )<br>
Insert the password and git will be installed.

---

After this, clone this repository using `git clone https://gitlab.com/guichaguri/youparty.git`<br>

Insert the gitlab username and password and the core will be cloned.

With the project cloned, enter it's folder by typing `cd ./youparty`.

Then you must update the NPM in workspace typing `npm install`.

After that, you should type `node server.js` or `npm run start`

The project will be running on this route `localhost:4200` so you should type in your favorite browser based on chromium.

Then this message should appear.

<p align="center">
<img src="./assets_readme/message.png" width="70%">
</p>

## How to use the application 

to access the application you need to access this url: `https://youparty-dev.herokuapp.com/`<br>

When you start the application and access it through your browser, you are on the home page, where you can create a room or access an already created room with the code.
<p align="center">
<img src="./assets_readme/homePage.png" width="70%">
</p>

Once inside the application, you can send messages, post YouTube links.
<p align="center">
<img src="./assets_readme/room.png" width="70%">
</p>

The room access code is highlighted in the upper left corner of the screen, when hovering over the code it is possible to copy it to share with your friends.

## Chat Commands

Chat commands are a quick way of controlling the player without leaving the chat.

| Usage                 | Aliases      | Description                                                                           |
| --------------------- | ------------ | ------------------------------------------------------------------------------------- |
| `/play [name or url]` | `/p`, `/add` | Adds a video into the queue. If the name or URL is omitted, resumes playing the paused video. |
| `/pause`              |              | Pauses the video.                                                                     |
| `/resume`             |              | Resumes playing the video.                                                            |
| `/volume <percent>`   |              | Changes the volume, ranging from 0% to 100%.                                          |
| `/fullscreen`         |              | Toggles fullscreen.                                                                   |
| `/skip`               | `/next`      | Skips to the next video.                                                              |
| `/seek <timestamp>`   |              | Changes the video position. The timestamp should follow the format: `HH:MM:SS` and can optionally start with `+` or `-` to increment or decrement from the current timestamp |
| `/kick <username>`    |              | Kicks an user from the room.                                                          |
| `/promote <username>` |              | Promotes an user into a host.                                                         |

## Web Development Class

* Faculdade de Engenharia de Sorocaba

    * Projeto Aplicado à Engenharia de Computação VI

        * Professor: Marc Gonzalez Capdevila

* Link para o vídeo: https://www.youtube.com/watch?v=Be5zYgeBo3Q

## Developed by

- Flávio José Lucas da Silva - 190847 
- Higor da Silva Lins - 190218
- Guilherme de Oliveira Chaguri - 190356
- Gustavo Moreira de Mello - 180525 
- Leonardo José Ferreira Corrêa - 210726
