const { fetchSearch } = require("../youtube/api");

class RoomInteractor {

	constructor(roomList, socket) {
		this.roomList = roomList;
		this.socket = socket;
		this.currentRoom = undefined;
		this.lastSearch = 0;
	}

	/**
	 * Triggered when the user creates a new room
	 * 
	 * @param {string} username The username
	 */
	createRoom(username) {
		if (this.currentRoom) return;

		this.currentRoom = this.roomList.createRoom();
		this.currentRoom.addConnection(this.socket, username.trim(), true);
	}

	/**
	 * Triggered when the user joins a room
	 * 
	 * @param {string} username The username
	 * @param {string} code The room code
	 */
	joinRoom(username, code) {
		if (this.currentRoom) return;

		this.currentRoom = this.roomList.findRoom(code);

		if (!this.currentRoom) {
			throw new Error('The room does not exist');
		}

		this.currentRoom.addConnection(this.socket, username.trim(), false);
	}

	/**
	 * Triggered when the user leaves the room
	 */
	leaveRoom() {
		if (!this.currentRoom) return;

		this.currentRoom.removeConnection(this.socket);

		if (this.currentRoom.isEmpty()) {
			this.currentRoom.destroy();
			this.roomList.removeRoom(this.currentRoom.code);
		}

		this.currentRoom = undefined;
	}

	/**
	 * Triggered when the user sends a chat message
	 * 
	 * @param {string} message The chat message
	 */
	sendMessage(message) {
		if (!this.currentRoom) return;

		message = message.trim();

		if (!message) {
			throw new Error('Message content is missing');
		}

		this.currentRoom.sendMessage(this.socket, message);
	}

	/**
	 * Triggered when the user adds a video to the queue
	 * 
	 * @param {string} videoUrl The YouTube video url
	 */
	playVideo(videoUrl) {
		if (!this.currentRoom) return;

		return this.currentRoom.addVideo(this.socket, videoUrl);
	}

	/**
	 * Triggered when the user pauses the video
	 */
	pauseVideo() {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can pause videos.');
		}

		this.currentRoom.pauseVideo();
	}

	/**
	 * Triggered when the user resumes playing the video
	 */
	resumeVideo() {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can resume videos.');
		}

		this.currentRoom.resumeVideo();
	}

	/**
	 * Triggered when the user skips the video
	 */
	skipVideo() {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can skip videos.');
		}

		this.currentRoom.nextVideo();
	}

	/**
	 * Triggered when the user seeks to a position
	 * 
	 * @param {number} position The video position in milliseconds
	 */
	seekTo(position) {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can seek videos.');
		}

		this.currentRoom.seekTo(position);
	}

	/**
	 * Triggered when the user kicks another user
	 * 
	 * @param {string} userId The user to kick
	 */
	kickUser(userId) {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can kick users.');
		}

		this.currentRoom.kickUser(userId);
	}

	/**
	 * Triggered when the user promotes another user to host
	 * 
	 * @param {string} userId The user to promote
	 */
	promoteUser(userId) {
		if (!this.currentRoom) return;

		if (!this.currentRoom.isHost(this.socket)) {
			throw new Error('Only a host of the room can promote users.');
		}

		this.currentRoom.promoteUser(userId);
	}

	/**
	 * Triggered when the user searches videos to add
	 * 
	 * @param {string} query The search query
	 */
	async searchVideos(query) {
		if (!this.currentRoom) return;
		if (!query) return;

		const now = Date.now();

		// Limit one search per second to prevent API abuse
		if (now - this.lastSearch < 1000) return;

		this.lastSearch = now;

		const results = await fetchSearch(query, 3);

		this.socket.emit('search-results', results);
	}

}

module.exports = RoomInteractor;
