const environment = require('../environment');
const { getVideoId } = require('../youtube/utils');
const { fetchVideoInfo, fetchSearch } = require('../youtube/api');

/**
 * Represents an on-going watch room
 */
class Room {

  constructor(io, code) {
    this.io = io;
    this.code = code;
    this.playlist = [];
    this.users = [];

    this.currentVideo = undefined;
    this.videoStartTime = 0;
    this.videoStartPosition = 0; // ms
    this.isPaused = false;

    this.videoEndTimeout = undefined;

    this.bufferMilliseconds = 5000;
  }

  _createRoomProxy() {
    return {
      code: this.code,
      users: this.users.map(user => ({ id: user.id, username: user.username, isHost: user.isHost })),
      playlist: this.playlist,
    };
  }

  _createVideoProxy() {
    return {
      videoId: this.currentVideo.videoId,
      user: this.currentVideo.user,
      info: this.currentVideo.info,
      startTime: this.videoStartTime,
      startPosition: this.videoStartPosition,
      isPaused: this.isPaused,
    };
  }

  /**
   * Adds a websocket client to the room.
   * This also dispatches room-based packets
   * 
   * @param {any} socket 
   * @param {string} username 
   * @param {boolean} isHost 
   */
  addConnection(socket, username, isHost) {
    if (username.length > environment.maxUsernameLength) {
      throw new Error('The username length must be below ' + environment.maxUsernameLength + ' characters');
    }

    if (this.users.find(user => user.username.toLowerCase() === username.toLowerCase())) {
      throw new Error('There is already a user connected in with this name');
    }

    console.log('Socket ' + socket.id + ' has joined ' + this.code + ' as ' + username);

    this.users.push({
      id: socket.id,
      socket: socket,
      username: username,
      isHost: isHost,
    });

		socket.username = username;

    const roomProxy = this._createRoomProxy();
    
    this.io.to(this.code).emit('room-updated', roomProxy);
    
    socket.emit('room-joined', roomProxy);

    if (this.currentVideo)
      socket.emit('video-play', this._createVideoProxy());

    socket.join(this.code);
  }

  /**
   * Removes a websocket client from the room.
   * 
   * @param {any} socket 
   */
  removeConnection(socket) {
    console.log(`Socket ${socket.id} has disconnected from ${this.code}`);

    this.users = this.users.filter(user => user.id !== socket.id);

    socket.leave(this.code);
		socket.emit('room-left');

    this.io.to(this.code).emit('room-updated', this._createRoomProxy());

    if (this.users.length > 0 && !this.users.find(user => user.isHost)) {
      // In case there are no hosts left, we'll promote the oldest user in the room
      this.promoteUser(this.users[0].id);
    }
  }

  /**
   * Checks whether the room is empty
   * @returns {boolean}
   */
  isEmpty() {
    return this.users.length === 0;
  }

  /**
   * Finds a user object by its identification
   * @param {string} userId The user identification
   * @returns {any | undefined} The user object
   */
  findUser(userId) {
    return this.users.find(user => user.id === userId);
  }

  /**
   * Checks whether a socket is the host of the room
   * @param {any} socket 
   * @returns {boolean}
   */
  isHost(socket) {
    const user = this.findUser(socket.id);
    return user ? user.isHost : false;
  }

  /**
   * Sends a message to the chat
   * 
   * @param {any} socket The websocket client that sent the message
   * @param {string} message The message
   */
  sendMessage(socket, message) {
    if (message.length > environment.maxMessageLength) {
      throw new Error('The message length must be below ' + environment.maxMessageLength + ' characters');
    }

    console.log(`${socket.username}: ${message}`);

    this.io.to(this.code).emit('chat-message', {
      id: socket.id,
      username: socket.username,
      message: message.trim(),
    });

    // TODO temp - remove
    if (message.startsWith('https'))
      this.addVideo(socket, message)
        .catch(error => socket.emit('error', error.message));
  }

  /**
   * Starts playing a video
   * 
   * @param {number} startDelay The delay in milliseconds to start the video
   * @param {number} startPosition The position in milliseconds to start the video
   */
  setVideoPosition(startDelay, startPosition) {
    this.videoStartTime = Date.now() + startDelay;
    this.videoStartPosition = startPosition;

    if (this.videoEndTimeout) {
      clearTimeout(this.videoEndTimeout);
    }

    if (!this.isPaused) {
      this.videoEndTimeout = setTimeout(
        () => this.nextVideo(),
        this.currentVideo.info.duration * 1000 - startPosition + startDelay
      );
    }
  }

  /**
   * Adds a YouTube video URL to the queue.
   * 
   * @param {any} socket The socket that added the video
   * @param {string} url The YouTube URL or video title
   */
  async addVideo(socket, url) {
    const videoId = getVideoId(url);
    let result;

    if (!videoId) {
      // If the video ID couldn't be resolved from the URL, searches instead
      result = await fetchSearch(url, 1);
    } else {
      // Gets the video info
      result = await fetchVideoInfo([videoId]);
    }

    if (!result || result.length === 0) {
      throw new Error('Video not found');
    }

    const info = result[0];

    if (info.duration <= 0) {
      throw new Error('Livestreams are not supported');
    }

    this.playlist.push({ videoId, info, user: socket.id });

    // In case the playlist was empty before this video, starts it immediately
    if (!this.currentVideo) {
      this.nextVideo();
    } else {
      this.io.to(this.code).emit('room-updated', this._createRoomProxy());
    }
  }

  /**
   * Loads and plays the next video
   */
  nextVideo() {
    if (this.playlist.length === 0) {
      this.stopVideo();
      return;
    }

    const item = this.playlist.shift();
    const startDelay = this.bufferMilliseconds + 100;

    this.currentVideo = item;
    this.isPaused = false;

    console.log('Playing ' + item.info.title + ' with duration of ' + item.info.duration);

    this.setVideoPosition(startDelay, 0);

    this.io.to(this.code).emit('video-play', this._createVideoProxy());

    // The playlist has been updated, we'll emit an event to update the clients
    this.io.to(this.code).emit('room-updated', this._createRoomProxy());
  }

  /**
   * Stops playing the current video
   */
  stopVideo() {
    if (this.videoEndTimeout) {
      clearTimeout(this.videoEndTimeout);
    }
    
    this.currentVideo = undefined;
    this.videoStartTime = 0;
    this.videoStartPosition = 0;
    this.videoEndTimeout = undefined;

    this.io.to(this.code).emit('video-stop');
  }

  /**
   * Pauses the current video
   */
  pauseVideo() {
    if (this.isPaused) {
      return;
    }

    if (!this.currentVideo) {
      return;
    }

    if (this.videoEndTimeout) {
      clearTimeout(this.videoEndTimeout);
      this.videoEndTimeout = undefined;
    }

    this.isPaused = true;
    this.videoStartPosition += Date.now() - this.videoStartTime;

    this.io.to(this.code).emit('video-pause', this._createVideoProxy());
  }

  /**
   * Resumes playing the current video
   */
  resumeVideo() {
    if (!this.isPaused) {
      return;
    }

    if (!this.currentVideo) {
      return;
    }

    this.isPaused = false;

    this.setVideoPosition(100, this.videoStartPosition);

    this.io.to(this.code).emit('video-play', this._createVideoProxy());
  }

  /**
   * Seeks to a specific video position
   * 
   * @param {number} position The position in milliseconds
   */
  seekTo(position) {
    if (!this.currentVideo) {
      return;
    }

    if (position > this.currentVideo.info.duration * 1000 || position < 0) {
      throw new Error('Invalid video position');
    }

    this.setVideoPosition(100, position);

    this.io.to(this.code).emit('video-play', this._createVideoProxy());
  }

  /**
   * Kicks an user from the room
   * 
   * @param {string} userId The user identification
   */
  kickUser(userId) {
    const user = this.findUser(userId);

    if (!user) {
      throw new Error('User not found');
    }

    this.removeConnection(user.socket);

    user.socket.disconnect(true);
  }

  /**
   * Promotes an user into a host
   * 
   * @param {string} userId The user identification
   */
  promoteUser(userId) {
    const userIndex = this.findUser(userId);

    if (userIndex === -1) {
      throw new Error('User not found');
    }

    this.users[this.users.indexOf(userIndex)].isHost = true;

    // Since this user has been updated, we'll update the room
    this.io.to(this.code).emit('room-updated', this._createRoomProxy());
  }

  /**
   * Destroy and cleanup the room resources
   */
  destroy() {
    if (this.videoEndTimeout) {
      clearTimeout(this.videoEndTimeout);
      this.videoEndTimeout = undefined;
    }

    this.io.to(this.code).emit('room-left');
    this.io.socketsLeave(this.code);
  }

}

module.exports = Room;