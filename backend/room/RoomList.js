const crypto = require('crypto');

const Room = require('./Room.js');

/**
 * Represents a list of on-going rooms
 */
class RoomList {

    constructor(io) {
        this.io = io;
        this.list = [];
    }

    /**
     * Creates a new room
     * @returns {Room} The room instance
     */
    createRoom() {
        const code = this._generateCode();
        const room = new Room(this.io, code);

        this.list.push(room);

        return room;
    }

    /**
     * Searches a room by code
     * @param {string} code 
     * @returns {Room | undefined} The room instance
     */
    findRoom(code) {
        return this.list.find(room => room.code === code);
    }

    /**
     * Removes a room by code
     * @param {string} code 
     */
    removeRoom(code) {
        this.list = this.list.filter(room => room.code !== code);
    }

    _generateCode() {
        let code, count = 0;
        
        do {
            // Increases the byte amount every 5 loops, so it never runs out of room codes
            const byteAmount = 3 + Math.floor(count++ / 5);
            
            code = crypto.randomBytes(byteAmount).toString('hex').toUpperCase();

            // Loops until it finds a code that is not yet allocated to a room
        } while (this.findRoom(code));

        return code;
    }

}

module.exports = RoomList;