module.exports = {
    /**
     * The http server port
     */
    port: process.env.PORT || 4200,

    /**
     * The YouTube Data API v3 Key
     */
    youtubeApiKey: process.env.YOUTUBE_API_KEY || 'AIzaSyD5Y_ljJTXyeDP3UvohYJEy5Ukzv-n1Ux8',

    /**
     * The maximum length of usernames
     */
    maxUsernameLength: process.env.MAX_USERNAME_LENGTH || 30,

    /**
     * The maximum length of chat messages
     */
    maxMessageLength: process.env.MAX_MESSAGE_LENGTH || 140,
};
