
const { fetchJson, parseVideoDetails } = require('./utils');
const config = require('../environment');

/**
 * Fetches from the YouTube API the video details
 * @param {number[]} ids The video IDs
 * @returns {Promise<any[]>} An array of objects containing the video information
 */
async function fetchVideoInfo(ids) {
    const url = 'https://youtube.googleapis.com/youtube/v3/videos' +
        '?part=snippet,contentDetails,statistics' +
        '&key=' + encodeURIComponent(config.youtubeApiKey);

    const qs = ids.map(id => 'id=' + encodeURIComponent(id)).join('&');

    const data = await fetchJson(url + '&' + qs);

    return data.items.map(item => parseVideoDetails(item));
}

/**
 * Searches YouTube videos
 * @param {string} query The search query
 * @param {number} maxResults The maximum quantity of results
 * @returns {Promise<any[]>} An array of objects containing the video information
 */
async function fetchSearch(query, maxResults) {
    const url = 'https://youtube.googleapis.com/youtube/v3/search' +
        '?part=id&type=video' +
        '&maxResults=' + maxResults +
        '&q=' + encodeURIComponent(query) +
        '&key=' + encodeURIComponent(config.youtubeApiKey);

    const data = await fetchJson(url);

    const videoIds = data.items.map(item => item.id.videoId);

    return await fetchVideoInfo(videoIds);
}

/**
 * Fetches related videos
 * @param {string} id The original video ID
 * @returns {Promise<any[]>} An array of objects containing the video information
 */
async function fetchRelated(id, maxResults) {
    const url = 'https://youtube.googleapis.com/youtube/v3/search' +
        '?part=id&part=snippet&type=video' +
        '&maxResults=' + maxResults +
        '&relatedToVideoId=' + encodeURIComponent(id) +
        '&key=' + encodeURIComponent(config.youtubeApiKey);

    const data = await fetchJson(url);

    const videoIds = data.items.map(item => item.id.videoId);

    return await fetchVideoInfo(videoIds);
}

module.exports = {
    fetchVideoInfo,
    fetchSearch,
    fetchRelated,
};
