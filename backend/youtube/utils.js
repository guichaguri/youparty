const https = require('https');

/**
 * Fetches a JSON from a HTTPs REST API
 * @param {string} url The URL to fetch
 * @returns {Promise<any>} The JSON data
 */
function fetchJson(url) {
    return new Promise((resolve, reject) => {

        https.get(url, (res) => {
            let data = '';
    
            res.on('data', (chunk) => data += chunk);
            res.on('end', () => {
                if (res.statusCode >= 200 && res.statusCode < 300) {
                    resolve(JSON.parse(data));
                } else {
                    reject(new Error(data));
                }
            });
            
        }).on('error', (e) => {
            reject(e);
        });

    });
}

/**
 * Gets the video ID from a YouTube url
 * 
 * @param {string} url 
 * @returns {string} The video ID
 */
function getVideoId(url) {
    const idRegex = /^[a-zA-Z0-9-_]{11}$/;

    if (idRegex.test(url)) {
        // The URL is a video ID
        return url;
    }

    const urlRegex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([a-z-A-Z0-9-_]+)/;
    const match = urlRegex.exec(url);

    return match ? match[1] : undefined;
}

/**
 * Parses an YouTube Data API v3 object into a video object
 * @param {any} item The item from the YouTube Data API v3
 * @returns {any} The video object
 */
function parseVideoDetails(item) {
    const snippet = item.snippet;
    const details = item.contentDetails;

    const thumbnail = Object.values(snippet.thumbnails).sort((a, b) => a.width * a.height - b.width * b.height);

    return {
        id: item.id,
        title: snippet.title,
        channel: snippet.channelTitle,
        thumbnail: thumbnail[0],
        duration: parseISO8601(details.duration),
    };
}

/**
 * Parses an ISO 8601 time string
 * @param {string} duration The duration string
 * @returns {number} The number of seconds
 */
function parseISO8601(duration) {
    const match = duration.match(/P(\d+Y)?(\d+W)?(\d+D)?T(\d+H)?(\d+M)?(\d+S)?/);
    
    if (!match) {
        // Invalid duration
        return 0;
    }

    const digits = match.map(n => n ? parseInt(n.replace(/\D/, '')) : 0);

    return (((digits[1] * 365 + digits[2] * 7 + digits[3]) * 24 + digits[4]) * 60 + digits[5]) * 60 + digits[6];
}

module.exports = {
    fetchJson,
    parseISO8601,
    parseVideoDetails,
    getVideoId,
};
