const express = require('express');
const path = require('path');

const RoomList = require('./room/RoomList');
const Room = require('./room/Room');
const RoomInteractor = require('./room/RoomInteractor');
const env = require('./environment');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
	cors: {
		origin: '*',
		methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
		preflightContinue: false,
		optionsSuccessStatus: 204
	}
});

app.use(express.static(path.join(__dirname, '../frontend')));

server.listen(env.port);
console.log('Listening on port ' + env.port);

const roomList = new RoomList(io);

io.on('connection', socket => {
	const interactor = new RoomInteractor(roomList, socket);

	/**
	 * Adds an event listener, catching exceptions
	 * @param {string} event The event
	 * @param {Function} handler The listener function, can throw exceptions or return promises
	 */
	function on(event, handler) {
		socket.on(event, async function (...args) {
			try {
				await Promise.resolve(handler(...args));
			} catch (err) {
				socket.emit('error', err.message);
				console.error(err);
			}
		});
	}

	/**
	 * Triggered when the user creates a new room
	 */
	on('create-room', (username) => interactor.createRoom(username));

	/**
	 * Triggered when the user joins a room
	 */
	on('join-room', (username, code) => interactor.joinRoom(username, code));

	/**
	 * Triggered when the user leaves the room
	 */
	on('leave-room', () => interactor.leaveRoom());

	/**
	 * Triggered when the user sends a chat message
	 */
	on('send-message', (message) => interactor.sendMessage(message));

	/**
	 * Triggered when the user adds a video to the queue
	 */
	on('play-video', (videoUrl) => interactor.playVideo(videoUrl));

	/**
	 * Triggered when the user pauses the video
	 */
	on('pause-video', () => interactor.pauseVideo());

	/**
	 * Triggered when the user resumes playing the video
	 */
	on('resume-video', () => interactor.resumeVideo());

	/**
	 * Triggered when the user skips the video
	 */
	on('skip-video', () => interactor.skipVideo());

	/**
	 * Triggered when the user seeks to a position
	 */
	on('seek-video', (position) => interactor.seekTo(position));

	/**
	 * Triggered when the user kicks another user
	 */
	on('kick-user', (userId) => interactor.kickUser(userId));

	/**
	 * Triggered when the user promotes another user to host
	 */
	on('promote-user', (userId) => interactor.promoteUser(userId));

	/**
	 * Triggered when the user searches videos to add
	 */
	on('search-videos', (query) => interactor.searchVideos(query));

	/**
	 * Triggered when the user disconnects
	 */
	on('disconnect', () => interactor.leaveRoom());
});